/*global define*/

define(['localStorage'], function (Store) {
    'use strict';

    return {
        ENTER_KEY: 13
    };
});