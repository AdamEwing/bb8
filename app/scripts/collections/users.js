/*global define*/

define (['backbone', 'localStorage', 'models/user'], function (Backbone, Store, User) {
  'use strict';

  var Users = Backbone.Collection.extend({

    model: User,

    localStorage: new Store('users')

  });

  return new  Users;

});
