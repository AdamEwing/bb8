/*global define*/

define(['backbone', 'localStorage', 'models/message'], function (Backbone, Store, Message) {
  'use strict';

  return Backbone.Collection.extend({

    model: Message,

    localStorage: new Store('messages')  

  });

});