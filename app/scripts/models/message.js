/*global define*/

define(['backbone'], function (Backbone) {
  'use strict';

  var Message = Backbone.Model.extend({

    parse: function(response, options)  {
      return response;
    }
    
  });

  return Message;

});
