/*global define*/

define(['backbone'], function (Backbone) {
  'use strict';

  var User = Backbone.Model.extend({
    
    parse: function(response, options)  {
      return response;
    }
  });

  return User;

});
