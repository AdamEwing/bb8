/*global require*/
'use strict';

require.config({
  shim: {
    underscore: {
      exports: '_'
    },
    backbone: {
      deps: [
        'underscore',
        'jquery'
      ],
      exports: 'Backbone'
    },
    bootstrap: {
      deps: ['jquery'],
      exports: 'jquery'
    },
    handlebars: {
      exports: 'Handlebars'
    }
  },
  paths: {
    jquery: '../bower_components/jquery/dist/jquery',
    backbone: '../bower_components/backbone/backbone',
    underscore: '../bower_components/lodash/dist/lodash',
    bootstrap: '../bower_components/bootstrap-sass-official/assets/javascripts/bootstrap',
    handlebars: '../bower_components/handlebars/handlebars',
    localStorage: '../bower_components/backbone.localStorage/backbone.localStorage',
    text: '../bower_components/requirejs-text/text'
  }
});

require([
  'backbone',
  'routes/router',
  'collections/users',
  'common'
], function (Backbone, Workspace, Users, Common) {

  function getNewUserAttrs () {
    var nextId = Users.length + 1;
    var img = 'http://coolmonkeydeals.com/static/images/users/71-1436214917.png';

    return {
      name: 'User' + nextId,
      photo: img
    };
  }

  Users.fetch();

  var currentUser = Users.create(getNewUserAttrs());

  Common.CURRENT_USER_ID = currentUser.id;

  new Workspace();

  Backbone.history.start();

});
