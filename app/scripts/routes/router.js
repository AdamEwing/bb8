/*global define*/

define(['backbone', 
    'views/messenger', 
    'views/settings'
    ], function (Backbone, MessengerView, SettingsView) {
  'use strict';

  var Workspace = Backbone.Router.extend({
      routes: {
      'messenger': 'messenger',
      'settings': 'settings',
      '*path': 'defaultRoute'
    },

    defaultRoute: function (param) {
      this.navigate('#/messenger');
    },

    messenger: function (param) {
      new MessengerView();      
    },

    settings: function (param) {
      new SettingsView();
    }
  });

  return Workspace;

});
