/*global define*/

define(['backbone', 
        'handlebars', 
        'text!templates/message.hbs',
        'collections/users'
        ], function (Backbone, Handlebars, Template, Users) {
  'use strict';

  var MessageView = Backbone.View.extend({

    template: Handlebars.compile(Template),

    tagName: 'li',

    events: {},

    initialize: function () {
      this.listenTo(this.model, 'change', this.render);
      this.listenTo(this.model, 'destroy', this.remove);
    },

    render: function () {
      this.linkWithUsers();
      this.$el.html(this.template(this.model.toJSON()));

      return this;
    },

    linkWithUsers: function () {
      var userIdFromModel = this.model.get('user_id');
      var userName, userPhoto;

      Users.each(function (user) {
        if (userIdFromModel === user.get('id')) {
          userName = user.get('name');
          userPhoto = user.get('photo');
        }
      });

      this.model.set('userName', userName);
      this.model.set('userPhoto', userPhoto);
    }

  });

  return MessageView;

});
