/*global define, window*/

define(['backbone', 
        'handlebars', 
        'text!templates/messenger.hbs',
        'collections/messages',
        'views/message',
        'common',
        'collections/users'
        ], function (Backbone, Handlebars, MessengerTemplate, Messages, MessageView, Common, Users) {
  'use strict';

  var MessengerView = Backbone.View.extend({

    el: '#main',

    template: Handlebars.compile(MessengerTemplate),

    tagName: 'div',

    id: '#messenger',

    events: {
      'keypress #new-message-input': 'createOnEnter',
      'click #new-message-button': 'createMessage'
    },

    initialize: function () {

      this.render();

      this.$input = this.$('#new-message-input');
      this.$messageList = this.$('#message-list');

      this.messages = new Messages();

      this.listenTo(this.messages, 'reset', this.addAll);
      this.listenTo(this.messages, 'add', this.addOne);

      $(window).on('storage', {messages: this.messages}, this.refresh);

      this.messages.fetch({reset: true});

    },

    render: function () {
      this.$el.html(this.template());
    },

    // Add a single message item to the list by creating a view for it, and
    // appending its element to the `<ul>`.
    addOne: function (message) {
      var view = new MessageView({ model: message });

      this.$messageList = this.$messageList || this.$('#message-list');

      this.$messageList.append(view.render().el);
    },

    // Add all items in the **Messages** collection at once.
    addAll: function () {
      this.$messageList.empty();
      this.messages.each(this.addOne, this);
    },

    createMessage: function () { 
      this.$input = this.$input || this.$('#new-message-input');

      if (!this.$input.val().trim()) {return;}

      this.messages.create(this.newAttributes());
      this.$input.val('');
    },

    createOnEnter: function (e) {
      if (e.which !== Common.ENTER_KEY) {return;}

      this.createMessage();
    },

    newAttributes: function () {
      var input = this.$input.val().trim();
      var urlPattern = /^(https?:\/\/)?([\da-z\.-]+)\.([a-z\.]{2,6})([\/\w \.-]*)*\/?$/;
      var inputArray = input.split(' ');
      var images = [];

      inputArray.forEach(function (part) {
          if (urlPattern.test(part)) {
            images.push(part);
          }
      });

      return {
        body: input,
        images: images,
        user_id: Common.CURRENT_USER_ID
      };
    },

    refresh: function (e) {
        e.data.messages.fetch({reset: true});
        Users.fetch({reset: true});
    }

  });

  return MessengerView;

});

