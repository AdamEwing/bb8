/*global define*/

define(['backbone', 
        'handlebars', 
        'text!templates/settings.hbs',
        'common',
        'collections/users'
        ], function (Backbone, Handlebars, Template, Common, Users) {
  'use strict';

  var SettingsView = Backbone.View.extend({

    el: '#main',

    template: Handlebars.compile(Template),

    tagName: 'div',

    events: {
      'change #user-name-input': 'changeUserName',
      'change #user-photo-input': 'changeUserPhoto'
    },

    initialize: function () {      
      this.model = Users.findWhere({id: Common.CURRENT_USER_ID});
      this.render();
      this.$input = $('#user-name-input');
      this.$photoInput = $('#user-photo-input');
      this.listenTo(this.model, 'change', this.render);
    },

    render: function () {
      this.$el.html(this.template(this.model.toJSON()));
    },

    changeUserName: function (e) {
      this.$input = $('#user-name-input');

      if (!this.$input.val().trim()) {
        return;
      }

      this.model.set('name', this.$input.val().trim());
      this.model.save();
    },

    changeUserPhoto: function (e) {
      this.$photoInput = $('#user-photo-input');

      if (!this.$photoInput.val().trim()) {
        return;
      }

      this.model.set('photo', this.$photoInput.val().trim());
      this.model.save();
    }

  });

  return SettingsView;

});
